<?php
/**
 * Get the homepage image
 *
 * @package Omeka\Function\View\Head
 * @uses get_theme_option()
 * @return string|null
 */
function homepage_image($position)
{
    $img = get_theme_option('Homepage Image');
    if ($img) {
 	//return $img;
        $storage = Zend_Registry::get('storage');
        $uri = $storage->getUri($storage->getPathByType($img, 'theme_uploads'));
        return '<img id="homepage_image_'.$position.'" src="' . $uri . '" alt="Homepage Image" />';
    }
}

